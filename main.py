from typing import List

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def get_driver():
    """Returns a new instance of the Chrome driver."""
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    driver = webdriver.Chrome(options=options)
    return driver


def load_page(driver, url):
    """Loads the page at the given URL."""
    driver.get(url)


def accept_cookies(driver):
    """Clicks on the 'Accept cookies' button."""
    accept_button = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, '/html/body/div[6]/div[2]/div[1]/div[2]/div[2]/button[1]')))
    accept_button.click()


def get_links(driver) -> List[str]:
    """Loads the page corresponding to the first link on the page."""
    link_elements = driver.find_elements(By.XPATH, '//a[@class="ait-title"]')
    links = [link.get_attribute("href") for link in link_elements]
    filtered_links = [link for link in links if link.startswith("https://www.origo.hu")]
    return filtered_links


def get_article_text_from_link(driver, link):
    # for link in links:
    load_page(driver, link)
    try:
        p_tags = driver.find_elements(By.TAG_NAME, 'p')
        tag_texts: List[str] = [p_tag.text for p_tag in p_tags]
        return ''.join(tag_texts)
    except Exception as e:
        print(f"Exception: {e!r}")


def main():
    # Initialize the driver and load the page
    driver = get_driver()
    load_page(driver, 'https://www.origo.hu')

    # Accept cookies
    accept_cookies(driver)

    # Load the site corresponding to the first link
    links = get_links(driver)
    article_texts: List[str] = []
    for link in links:
        # TODO: Remove these debug logs
        print(link)
        article_text = get_article_text_from_link(driver, link)
        print(article_text)
        article_texts.append(article_text)
        print('\n')


    # Clean up
    driver.quit()


if __name__ == '__main__':
    main()
